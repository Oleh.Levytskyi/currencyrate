package com.example.currencyrate.converter;

import com.example.currencyrate.model.Currency;
import com.example.currencyrate.model.CurrencyRate;
import com.example.currencyrate.web.dto.CurrencyRateDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class DtoToCurrencyRateConverter {
    public CurrencyRate convert(CurrencyRateDto currencyRateDto, List<Currency> currencyList) {
        CurrencyRate currencyRate = CurrencyRate.builder()
                .amount(currencyRateDto.getAmount())
                .cnbMid(currencyRateDto.getCnbMid())
                .currBuy(currencyRateDto.getCurrBuy())
                .currMid(currencyRateDto.getCurrMid())
                .currSell(currencyRateDto.getCurrSell())
                .move(currencyRateDto.getMove())
                .valBuy(currencyRateDto.getValBuy())
                .validFrom(currencyRateDto.getValidFrom())
                .valMid(currencyRateDto.getValMid())
                .valSell(currencyRateDto.getValSell())
                .version(currencyRateDto.getVersion())
                .build();
        Currency currency = currencyList.stream()
                .filter(curr -> curr.getShortName().equals(currencyRateDto.getShortName()))
                .findFirst()
                .orElse(new Currency());
        currency.setName(currencyRateDto.getName());
        currency.setCountry(currencyRateDto.getCountry());
        currency.setShortName(currencyRateDto.getShortName());
        currencyRate.setCurrency(currency);
        currencyRate.setId(UUID.randomUUID());
        currencyRate.setLastUpdatedTime(currencyRateDto.getLastUpdatedTime());
        return currencyRate;
    }
}
