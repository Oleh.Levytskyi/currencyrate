package com.example.currencyrate.converter;

import com.example.currencyrate.model.CurrencyRate;
import com.example.currencyrate.web.dto.CurrencyRateDto;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CurrencyRateToDtoConverter {
    public CurrencyRateDto convert(CurrencyRate currencyRate) {
        return CurrencyRateDto.builder()
                .amount(currencyRate.getAmount().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .cnbMid(currencyRate.getCnbMid().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .country(currencyRate.getCurrency().getCountry())
                .currBuy(currencyRate.getCurrBuy().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .currMid(currencyRate.getCurrMid().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .currSell(currencyRate.getCurrSell().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .move(currencyRate.getMove().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .name(currencyRate.getCurrency().getName())
                .shortName(currencyRate.getCurrency().getShortName())
                .valBuy(currencyRate.getValBuy().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .validFrom(currencyRate.getValidFrom())
                .valMid(currencyRate.getValMid().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .valSell(currencyRate.getValSell().setScale(3, BigDecimal.ROUND_HALF_EVEN))
                .version(currencyRate.getVersion())
                .lastUpdatedTime(currencyRate.getLastUpdatedTime())
                .build();
    }
}
