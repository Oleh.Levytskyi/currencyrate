package com.example.currencyrate.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Currency {
    @Id
    private UUID id = UUID.randomUUID();
    private String country;
    private String name;
    private String shortName;

//    @OneToMany (
//            mappedBy = "currency_rate",
//            orphanRemoval = true,
//            cascade = CascadeType.ALL
//    )
//    private List<CurrencyRate> currencyRates;
}
