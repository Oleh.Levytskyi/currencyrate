package com.example.currencyrate.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class CurrencyRate {
    @Id
    private UUID id = UUID.randomUUID();
    private BigDecimal amount;
    private BigDecimal cnbMid;
    private BigDecimal currBuy;
    private BigDecimal currMid;
    private BigDecimal currSell;
    private BigDecimal move;
    private BigDecimal valBuy;
    private BigDecimal valMid;
    private BigDecimal valSell;
    private LocalDateTime validFrom;
    private String version;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;
    private LocalDateTime lastUpdatedTime;
}
