package com.example.currencyrate.web.controller.rest;

import com.example.currencyrate.service.CurrencyRateService;
import com.example.currencyrate.web.dto.CurrencyRateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
public class CurrencyRateRestController {

    @Autowired
    private CurrencyRateService currencyRateService;

    @GetMapping("loadData")
    public @ResponseBody
    List<CurrencyRateDto> getDankData(@RequestParam(value = "usedb") Boolean usedb) {
        return currencyRateService.getRates(usedb);
    }


}
