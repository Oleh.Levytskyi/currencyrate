package com.example.currencyrate.web.controller;

import com.example.currencyrate.service.CurrencyRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CurrencyRateController {

    @Autowired
    private CurrencyRateService currencyRateService;

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("currencyRateDtos", currencyRateService.findLastRatesInDb());
        return "index";
    }
}
