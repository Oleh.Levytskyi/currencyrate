package com.example.currencyrate.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CurrencyRateDto implements Serializable {
    private String country;
    private String name;
    private String shortName;
    private BigDecimal amount;
    private BigDecimal cnbMid;
    private BigDecimal currBuy;
    private BigDecimal currMid;
    private BigDecimal currSell;
    private BigDecimal move;
    private BigDecimal valBuy;
    private BigDecimal valMid;
    private BigDecimal valSell;
    private LocalDateTime validFrom;
    private String version;
    private LocalDateTime lastUpdatedTime;
}
