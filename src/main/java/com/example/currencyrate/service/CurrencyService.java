package com.example.currencyrate.service;

import com.example.currencyrate.model.Currency;

import java.util.List;
import java.util.UUID;

public interface CurrencyService {
    Currency add(Currency currency);
    List<Currency> findAll();
    Currency findById(UUID id);
    Currency findByShortName(String shortName);
}
