package com.example.currencyrate.service;

import com.example.currencyrate.model.CurrencyRate;
import com.example.currencyrate.web.dto.CurrencyRateDto;

import java.util.List;
import java.util.UUID;

public interface CurrencyRateService {

    CurrencyRate add(CurrencyRate currencyRate);
    List<CurrencyRate> findAll();
    CurrencyRate findById(UUID id);
    List<CurrencyRateDto> loadRates();
    List<CurrencyRateDto> saveRates(List<CurrencyRateDto> currencyRateDtos);
    List<CurrencyRateDto> getRates(Boolean useExternalService);
    List<CurrencyRateDto> findLastRatesInDb();
}
