package com.example.currencyrate.service;

import com.example.currencyrate.web.dto.CurrencyRateDto;

import java.util.List;

public interface BankService {
    List<CurrencyRateDto> getLastData();
}
