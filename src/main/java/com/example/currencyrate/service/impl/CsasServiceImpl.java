package com.example.currencyrate.service.impl;

import com.example.currencyrate.service.BankService;
import com.example.currencyrate.web.dto.CurrencyRateDto;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class CsasServiceImpl implements BankService {

    private String URL = "https://www.csast.csas.cz/webapi/api/v1/rates/exchangerates?web-api-key=";
    private String API_KEY = "86d63706-3a9c-4762-bd7a-415651cc26f8";
    private String FULL_URL = "https://www.csast.csas.cz/webapi/api/v1/rates/exchangerates?web-api-key=86d63706-3a9c-4762-bd7a-415651cc26f8";

    private final RestTemplate restTemplate;

    public CsasServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public List<CurrencyRateDto> getLastData() {
        try {
            List<CurrencyRateDto> currencyRateDtos = Arrays.asList(this.restTemplate.getForObject(URL + API_KEY, CurrencyRateDto[].class));
            LocalDateTime nowDateTime = LocalDateTime.now();
            for (CurrencyRateDto currencyRateDto : currencyRateDtos) {
                currencyRateDto.setLastUpdatedTime(nowDateTime);
            }
            return currencyRateDtos;
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }
}
