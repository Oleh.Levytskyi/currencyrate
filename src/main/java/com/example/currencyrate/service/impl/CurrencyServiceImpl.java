package com.example.currencyrate.service.impl;

import com.example.currencyrate.model.Currency;
import com.example.currencyrate.repository.CurrencyRepository;
import com.example.currencyrate.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    @Autowired
    private CurrencyRepository currencyRepository;

    @Override
    public Currency add(Currency currency) {
        return currencyRepository.save(currency);
    }

    @Override
    public List<Currency> findAll() {
        return (List<Currency>) currencyRepository.findAll();
    }

    @Override
    public Currency findById(UUID id) {
        return currencyRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Currency findByShortName(String shortName) {
        return currencyRepository.findByShortName(shortName);
    }
}
