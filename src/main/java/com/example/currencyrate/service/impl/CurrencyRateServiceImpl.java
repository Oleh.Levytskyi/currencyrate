package com.example.currencyrate.service.impl;

import com.example.currencyrate.converter.CurrencyRateToDtoConverter;
import com.example.currencyrate.converter.DtoToCurrencyRateConverter;
import com.example.currencyrate.model.Currency;
import com.example.currencyrate.model.CurrencyRate;
import com.example.currencyrate.repository.CurrencyRateRepository;
import com.example.currencyrate.repository.CurrencyRepository;
import com.example.currencyrate.service.BankService;
import com.example.currencyrate.service.CurrencyRateService;
import com.example.currencyrate.service.CurrencyService;
import com.example.currencyrate.web.dto.CurrencyRateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CurrencyRateServiceImpl implements CurrencyRateService {

    @Autowired
    private BankService bankService;

    @Autowired
    private CurrencyRateRepository currencyRateRepository;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private DtoToCurrencyRateConverter dtoToCurrencyRateConverter;

    @Autowired
    private CurrencyRateToDtoConverter currencyRateToDtoConverter;

    @Override
    public CurrencyRate add(CurrencyRate currencyRate) {
        return currencyRateRepository.save(currencyRate);
    }

    @Override
    public List<CurrencyRate> findAll() {
        return (List<CurrencyRate>) currencyRateRepository.findAll();
    }

    @Override
    public CurrencyRate findById(UUID id) {
        return currencyRateRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Transactional
    public List<CurrencyRateDto> loadRates() {
        return bankService.getLastData();
    }

    @Override
    public List<CurrencyRateDto> saveRates(List<CurrencyRateDto> currencyRateDtoList) {
        List<CurrencyRate> result = new ArrayList<>();
        List<Currency> currencyList = currencyService.findAll();
        for (CurrencyRateDto currencyRateDto : currencyRateDtoList) {
            CurrencyRate currencyRate = dtoToCurrencyRateConverter.convert(currencyRateDto, currencyList);
            CurrencyRate persistenceCurrencyRate = currencyRateRepository.findByShortNameAndValidFrom(currencyRateDto.getShortName(), currencyRateDto.getValidFrom());
            if (persistenceCurrencyRate != null) {
                currencyRateRepository.delete(persistenceCurrencyRate);
            }
            currencyRepository.save(currencyRate.getCurrency());
            currencyRateRepository.save(currencyRate);
            result.add(currencyRate);
        }
        return result.stream().
                map(currencyRate -> currencyRateToDtoConverter.convert(currencyRate))
                .collect(Collectors.toList());
    }

    @Override
    public List<CurrencyRateDto> getRates(Boolean usedb) {
        if (usedb) {
            return findLastRatesInDb();
        }
        else {
            return loadRates();
        }
    }

    @Override
    public List<CurrencyRateDto> findLastRatesInDb() {
        List<CurrencyRateDto> result = new ArrayList<>();
        List<Currency> currencyList = currencyService.findAll();
        for(Currency currency : currencyList) {
            List<CurrencyRate> currencyRateList = currencyRateRepository.findByCurrency(currency);
            CurrencyRate currencyRate = currencyRateList.stream()
                    .max(Comparator.comparing(CurrencyRate::getValidFrom))
                    .get();
            result.add(currencyRateToDtoConverter.convert(currencyRate));
        }
        return result;
    }
}
