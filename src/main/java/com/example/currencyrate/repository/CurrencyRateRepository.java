package com.example.currencyrate.repository;

import com.example.currencyrate.model.Currency;
import com.example.currencyrate.model.CurrencyRate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface CurrencyRateRepository extends CrudRepository<CurrencyRate, UUID> {
    @Query(value = "select * from currency_rate as cr " +
            "left join currency c on cr.currency_id = c.id " +
            "where cr.valid_from = :validFrom and c.short_name = :shortName", nativeQuery = true)
    CurrencyRate findByShortNameAndValidFrom(@Param("shortName") String shortName,
                                             @Param("validFrom") LocalDateTime validFrom);

//    @Query(value = "select * from currency_rate as cr " +
//            "left join currency c on cr.currency_id = c.id " +
//            "where cr.last_updated_time = :lastUpdatedTime and c.short_name = :shortName", nativeQuery = true)
//    CurrencyRate findByShortNameAndLastUpdatedTime(@Param("shortName") String shortName,
//                                             @Param("validFrom") LocalDateTime lastUpdatedTime);

    @Query(value = "select * from currency_rate as cr " +
            "left join currency c on cr.currency_id = c.id " +
            "where c.short_name = :shortName", nativeQuery = true)
    List<CurrencyRate> findByShortName(@Param("shortName") String shortName);

    List<CurrencyRate> findByCurrency(Currency currency);
}
