package com.example.currencyrate.job;

import com.example.currencyrate.service.CurrencyRateService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@DisallowConcurrentExecution
@Component
public class AutoupdateDataFromBank implements Job{

    @Autowired
    private CurrencyRateService currencyRateService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.printf(LocalDateTime.now() + "Autoupdate data from external service");
        currencyRateService.saveRates(currencyRateService.loadRates());
    }
}