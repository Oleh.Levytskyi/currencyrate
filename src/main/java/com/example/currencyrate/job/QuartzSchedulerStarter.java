package com.example.currencyrate.job;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

@Component
public class QuartzSchedulerStarter {
    @Value("${quartz.autoupdateCurrencyRate.frequency.in.sec}")
    private Integer autoupdateCurrencyRateFrequencyInSec;

    @Autowired
    private Scheduler scheduler;

    @PostConstruct
    private void schedulerStarter() throws SchedulerException {
        scheduler.scheduleJob(jobDetail(), autoupdateTrigger(jobDetail()));
        scheduler.start();
    }

    private JobDetail jobDetail() {
        return newJob().ofType(AutoupdateDataFromBank.class)
                .storeDurably()
                .withIdentity(JobKey.jobKey("Autoupdate_Job_Detail"))
                .build();
    }

    private Trigger autoupdateTrigger(JobDetail job) {
        return newTrigger().forJob(job)
                .withIdentity(TriggerKey.triggerKey("Autoupdate_Trigger"))
                .withSchedule(simpleSchedule().withIntervalInSeconds(autoupdateCurrencyRateFrequencyInSec).repeatForever())
                .build();
    }
}
