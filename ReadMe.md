This is the Currency Rate application.

To run this application you need:

1. docker run --rm -e POSTGRES_PASSWORD=mysecretpassword -d -p 5433:5432 postgres:9.6.17-alpine
2. download the sources and from the main folder run "mvn clean package"
3. java -jar target/currencyrate-0.0.1-SNAPSHOT.jar
4. open http://localhost:8080/
4a. for swagger open http://localhost:8080/swagger-ui.html